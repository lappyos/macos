# MacOS public repo

This is a public repo to setup fresh MacOS for my bloody relatives


# Macbooks

Get home brew

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

from https://brew.sh/

## Updating in Homebrew

```
brew update; brew upgrade --greedy; brew cleanup
```

### Get list of applcations installed on MacOS with Homebrew

```
brew bundle dump --describe
```

### To restore MacOS software and homebrew apps from a Brewfile


cd into where the Brewfile is located first

```
sudo softwareupdate -ia -verbose ; brew bundle -v ; brew cleanup ; brew doctor
```

from https://twit.tv/shows/hands-on-mac/episodes/9

```
brew bundle install --file /path/to/Brewfile
```


# Windows

Get The Ultimate Windows Utility

```
iwr -useb https://christitus.com/win | iex
```

from https://christitus.com/windows-tool/


Get chocolatey

Check First:

With PowerShell, you must ensure Get-ExecutionPolicy is not Restricted. We suggest using Bypass to bypass the policy to get things installed or AllSigned for quite a bit more security.

Run Get-ExecutionPolicy. If it returns Restricted, then run Set-ExecutionPolicy AllSigned or Set-ExecutionPolicy Bypass -Scope Process.

then run

```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

from https://chocolatey.org/


## Upgrading Chocolatey

Once installed, Chocolatey can be upgraded in exactly the same way as any other package that has been installed using Chocolatey. Simply use the command to upgrade to the latest stable release of Chocolatey:

```
choco upgrade chocolatey
```

## Upgrade everything in Chocolatey

```
choco upgrade all
```