Configuration details for Quad9's Recommended service are below:

IPv4 Primary - 9.9.9.9

IPv4 Secondary - 149.112.112.112

IPv6 Primary - 2620:fe::fe

IPv6 Secondary - 2620:fe::9

DoT - dns.quad9.net

DoH -

https://dns.quad9.net/dns-query

DNSCrypt - quad9-dnscrypt-ip4-filter-pri & quad9-dnscrypt-ip6-filter-pri

Confirm you using Quad9 
https://on.quad9.net 
